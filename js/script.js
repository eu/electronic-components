function throttle(func, ms) {
    var isThrottled = false, savedArgs, savedThis;
    function wrapper() {
        if (isThrottled) {
            savedArgs = arguments;
            savedThis = this;
            return;
        }
        func.apply(this, arguments);
        isThrottled = true;
        setTimeout(function() {
            isThrottled = false;
            if (savedArgs) {
                wrapper.apply(savedThis, savedArgs);
                savedArgs = savedThis = null;
            }
        }, ms);
    }
    return wrapper;
}

function urlEncode(str)
{
    return encodeURIComponent(str.replace(/\+/g, "%2B"));
}

function ajaxRequest(url, opt, onSuccess, onFail)
{
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                if (onSuccess !== undefined)
                    onSuccess(xmlhttp.responseText);
                return;
            }
            if (onFail !== undefined)
                onFail();
        }
    }
    if (opt !== undefined) {
        xmlhttp.open("POST", url, true);
        xmlhttp.setRequestHeader("Content-type", 
                                 "application/x-www-form-urlencoded");
        xmlhttp.send(opt);
    } else {
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }
}

function _suggest(s)
{
    var props = document.getElementById('Props');

    if (typeof s === 'undefined') {
        if (props.innerHTML.trim() !== '') {
            props.style.display = 'flex';
        }
        return;
    }

    if (s.length < 3) {
        props.style.display = 'none';
        return;
    }

    ajaxRequest("", "s="+urlEncode(s), 
        function(html) {
            props.innerHTML = html;
            props.style.display = (html) ? 'flex' : 'none';
        },
        function() {
            props.innerHTML = 'error';
        }
    );
}

function closePopups()
{
    document.getElementById('Props').style.display='none';
}

function validateLoginForm(token)
{
    var md5pass = md5(document.forms["LoginForm"]["password"].value);
    document.forms["LoginForm"]["password"].value = md5(token + md5pass);
    document.forms["LoginForm"].style.display='none';
    return true;
}

function delConfirm()
{
    return confirm("ТОЧНО УДАЛЯЕМ?");
}

var suggest = throttle(_suggest, 1000);
document.getElementById('s').addEventListener('click', 
    function(event) {
        suggest();
        event.stopPropagation();
    }
);
document.getElementById('s').addEventListener('keyup', 
    function() {
        suggest(this.value)
    }
);

