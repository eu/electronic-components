function saveContent(id, content)
{
	var area = document.getElementById(id);
	
	if (typeof content === 'undefined') {
		alert('Ошибка при получении контента!');
		return;
	}
	ajaxRequest("admin/ce_save/"+id, "html="+urlEncode(content), 
		function(html) { 
			area.innerHTML = html;
			alert("Сохранено.");
		},
		function() { alert('Ошибка при сохранении контента!'); }
	);
}

