#!/bin/sh
WWW=/tmp/www

rm -rf $WWW
mkdir $WWW

find . -type f \( -name '*.swp' -o -name '*.swo' -o -name '*~' -o -name '*.bak' -o -name '.netrwhist' \) -delete
cp -R src $WWW/
cp -R css $WWW/
mkdir $WWW/img
cp img/* $WWW/img/
cp -r js  $WWW/
cp favicon.ico index.php .htaccess robots.txt sitemap.xml *.html $WWW

cd $WWW
mkdir tmp download img/ce
rm src/cache/*
rm src/view/ce/*
NOW=`date +%Y-%m-%d`
sed -i "
s/<lastmod>.*<\/lastmod>/<lastmod>$NOW<\/lastmod>/
" sitemap.xml

sed -i "
s/define('APP_ENV', 'DEV'/define('APP_ENV', 'PROD'/
" src/config/cfg.php

tar cvzf /tmp/www.tgz .
cd /tmp
rm -rf www

