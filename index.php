<?php declare(strict_types=1);

define('__SRC__', 'src/');

$time = microtime(true);

require_once __SRC__.'config/cfg.php';
require_once __SRC__.'app.php';

app();

$time = number_format((float) microtime(true)-$time, 4, ',', ' ');
$mem = (int) (memory_get_peak_usage(true) / 1024);
echo "<!-- Сompleted in $time sec. Peak of allocated memory: $mem Kb -->";
?>
