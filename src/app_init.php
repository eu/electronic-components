<?php declare(strict_types=1);

require_once __INC__.'asset.php';
require_once __INC__.'session.php';
require_once __INC__.'sanitize.php';
require_once __INC__.'flash.php';
require_once __INC__.'db.php';
require_once __MODEL__.'role.php';
require_once __MODEL__.'exchange.php';

//Profile init
if (!session_get('profile')) {
    session_set('currency', 'RUB');
    session_set('exchange', exchange());
    role_set(ROLE_NOBODY);
    session_set('base_uri', 'base');
}

//Set default values
$exchange = session_get('exchange');
$profile  = session_get('profile');
$v = [
    'view'        => 'index.phtml',
    'title'       => SITE_TITLE,
    'description' => SITE_DESCRIPTION,
    'keywords'    => SITE_KEYWORDS,
    'robots' =>      'index, follow',

    'og' => [
        'url'   => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                    "https" : "http").
                    "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",

        'type'  => 'website',

        'image' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                    "https" : "http").
                    "://$_SERVER[HTTP_HOST]".ROOT.'img/logo.png'
    ],

    'css'        => ROOT.'css/'.s(asset()['styles.css']),
    'js'         => ROOT.'js/script.js?v=2',

    'cy_symb' => [
        //'RUB' => '&#8381;',
        'RUB' => 'Р',
        'USD' => '$',
        'UAH' => '&#8372;'
    ],

    'currency'   => session_get('currency'),
    'role'       => (int) $profile['role'],
    'token'      => session_get('token'),
    'base_uri'   => session_get('base_uri'),
    'search'     => ''
];
$v['exch'] = (float) $exchange[ $v['currency'] ];
$v['cy']   = $v['cy_symb'][ $v['currency'] ];

foreach ($profile as $key => $val) {
    $v['profile'][s($key)] = s($val);
}

if ($v['role'] >= ROLE_SELLER) {
    define('DB_USER', DB_ADMIN_NAME);
    define('DB_PASS', DB_ADMIN_PASS);
}

unset($exchange, $profile);
