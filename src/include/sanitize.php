<?php declare(strict_types=1);

function s($str): string
{
    return htmlentities(trim("$str"), ENT_QUOTES, 'UTF-8');
}
