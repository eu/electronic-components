<?php declare(strict_types=1);

defined('__CACHE__') || define('__CACHE__', __SRC__.'cache/');

function cache_key(string $key): string
{
    return preg_replace("/[^a-z0-9\-_\*]+/i", "", $key);
}

function cache_save(string $key, array &$data): bool
{
    $cache = __CACHE__.cache_key($key);
    $file = @fopen($cache, 'w');
    if (!$file)
        return false;

    if (flock($file, LOCK_EX)) {
        if (!fwrite($file, json_encode($data, JSON_UNESCAPED_UNICODE)))
            return false;
        fflush($file);
        flock($file, LOCK_UN);
    } else {
        fclose($file);
        return false;
    }
    fclose($file);
    return true;
}

function cache_get(string $key): ?array
{
    $cache = __CACHE__.cache_key($key);
    if (!file_exists($cache)) return null;
    $file = @fopen($cache, 'r');
    if (!$file)
        return null;

    if (flock($file, LOCK_SH)) {
        $json = fread($file, filesize($cache));
        if (!$json)
            return null;
        flock($file, LOCK_UN);
    } else {
        fclose($file);
        return null;
    }
    fclose($file);

    $data = json_decode($json, true);
    return $data;
}

function cache_delete(string $key): bool
{
    $cache = __CACHE__.cache_key($key);
    return (file_exists($cache)) ?
            unlink($cache) : (bool) array_map('unlink', glob($cache));
}
