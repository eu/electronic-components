<?php declare(strict_types=1);

defined('SESSID') || define('SESSID', '');

function session_init(): void
{
    if (session_status() != PHP_SESSION_NONE)
        return;

    session_name(SESSID);
    session_start();

    if (isset($_SESSION['id']))
        return;

    session_unset();
    session_regenerate_id();
    $_SESSION['id'] = session_id();
    session_new_token();
}

function session_get($name)
{
    session_init();
    return $_SESSION[$name] ?? null;
}

function session_set($name, $value): void
{
    session_init();
    $_SESSION[$name] = $value;
}

function session_add($name, $value): void
{
    session_init();
    $_SESSION[$name][] = $value;
}

function session_new_token(): void
{
    $_SESSION['token'] = md5(uniqid((string) rand(), true));
}
