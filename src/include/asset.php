<?php declare(strict_types=1);

function asset(): array
{
    $json = file_get_contents(__VIEW__.'manifest.json');

    if ($json !== false) {
        $json = utf8_encode($json);
        $asset = json_decode($json, true);
        if (json_last_error() !== JSON_ERROR_NONE)
            $asset['styles.css'] = 'styles.css';
    }
    return $asset;
}
