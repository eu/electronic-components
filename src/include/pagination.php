<?php declare(strict_types=1);

require_once __INC__.'db.php';

function pagination_count_pages(string $table, int $on_page,
                                string $filter='', string $join=''): int
{
    $sql = "SELECT COUNT(*) FROM `$table` ";

    if ($join)
        $sql .= "$join ";

    if ($filter)
        $sql .= "WHERE $filter ";

    $sql .= 'LIMIT 1';

    $res = db_query($sql);
    if (!$res)
        return 0;

    $records = db_fetch_row($res);
    db_free($res);

    return (int) ceil($records[0] / $on_page);
}
