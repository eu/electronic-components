<?php declare(strict_types=1);

defined('COOKIE_NAME') || define('COOKIE_NAME', 'COOKIE');
defined('COOKIE_LIFETIME') || define('COOKIE_LIFETIME', 3600);

function cookie_get(): ?string
{
    return $_COOKIE[COOKIE_NAME] ?? null;
}

function cookie_set(string $val)
{
    $_COOKIE[COOKIE_NAME] = $val;
    setcookie(COOKIE_NAME, $val, time()+COOKIE_LIFETIME, '/', '', false, true);
}

function cookie_delete()
{
    unset($_COOKIE[COOKIE_NAME]);
    setcookie(COOKIE_NAME, '', time()-3600, '/', '', false, true);
}
