<?php declare(strict_types=1);
/*
 * Database functions
 *
 * use global $db_connection
 * use defines: DB_HOST, DB_NAME, DB_USER, DB_PASS
 */
function db_set_connection($connection) {
    global $db_connection;
    $db_connection = $connection;
}

function db_get_connection() {
    global $db_connection;
    (isset($db_connection)) || die('DB connection not established!');
    return $db_connection;
}

function db_is_connected() {
    global $db_connection;
    return isset($db_connection);
}

function db_close_connection() {
    global $db_connection;
    if (mysqli_close($db_connection))
    	unset($GLOBALS['db_connection']);
}

function db_open()
{
    defined('DB_USER') || define('DB_USER', DB_USER_NAME);
    defined('DB_PASS') || define('DB_PASS', DB_USER_PASS);

    if (db_is_connected())
        return;

    $connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	if (mysqli_connect_errno())
        die('Database connection failed with error: '.mysqli_connect_error());

	mysqli_query($connection, 'set character_set_client="utf8"');
	mysqli_query($connection, 'set character_set_results="utf8"');
    mysqli_query($connection, 'set collation_connection="utf8_general_ci"');

    db_set_connection($connection);
}

function db_insert_id()
{
    return mysqli_insert_id(db_get_connection());
}

function db_s($query)
{
    if (!db_is_connected())
        db_open();
	return mysqli_real_escape_string(db_get_connection(), trim($query));
}

function db_query($query)
{
    if (!db_is_connected())
        db_open();
	return mysqli_query(db_get_connection(), $query);
}

function db_autocommit_off()
{
    if (!db_is_connected())
        db_open();
	mysqli_autocommit(db_get_connection(), FALSE);
}

function db_commit()
{
	mysqli_commit(db_get_connection());
}

function db_rollback()
{
	mysqli_rollback(db_get_connection());
}

function db_fetch_assoc($res)
{
    return mysqli_fetch_assoc($res);
}

function db_fetch_row($res)
{
    return mysqli_fetch_row($res);
}

function db_num_rows($res)
{
    return mysqli_num_rows($res);
}

function db_affected_rows()
{
    return mysqli_affected_rows(db_get_connection());
}

function db_free(&$res)
{
	mysqli_free_result($res);
}

function db_sqlstate() {
    global $db_connection;
    return mysqli_sqlstate($db_connection);
}

function db_error()
{
    return mysqli_error(db_get_connection());
}

function db_errno()
{
    return mysqli_errno(db_get_connection());
}

function db_exit($file, $line, $msg='Ошибка при работе с базой данных!')
{
    db_close_connection();
	exit('['.basename($file)."@$line]".$msg);
}

function db_close()
{
    if (db_is_connected())
        db_close_connection();
}
