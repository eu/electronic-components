<?php declare(strict_types=1);

defined('LOCK_FILE') || define('LOCK_FILE', 'tmp/lock');

function lock(): void
{
    touch(LOCK_FILE);
}

function lock_isset(): bool
{
    return file_exists(LOCK_FILE);
}

function lock_free(): void
{
    unlink(LOCK_FILE);
}
