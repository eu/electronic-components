<?php declare(strict_types=1);

defined('__CE_PAGE__') || define('__CE_PAGE__', __VIEW__.'ce/');
defined('__CE_IMG__') || define('__CE_IMG__', 'img/ce/');

function ce_rewrite(string $file_name, string &$data): bool
{
    $file = @fopen(__CE_PAGE__.$file_name, 'w');
    if (!$file)
        return false;

    if (flock($file, LOCK_EX)) {
        if (!fwrite($file, $data))
            return false;
        fflush($file);
        flock($file, LOCK_UN);
    } else {
        fclose($file);
        return false;
    }
    fclose($file);
    return true;
}

