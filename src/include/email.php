<?php declare(strict_types=1);

defined('MAILBOT') || define('MAILBOT', 'mailbot@mail.ru');

function email(string $to, string $subj, string $body)
{
    return mail($to, '=?UTF-8?B?'.base64_encode($subj).'?=',$body,
        "MIME-Version: 1.0\r\nContent-type: text/plain; charset=utf-8\r\n".
        'From: '.MAILBOT."\r\nReply-To: ".ORDER_MAIL."\r\n");
}
