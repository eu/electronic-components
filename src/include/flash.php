<?php declare(strict_types=1);

function flash_note($msg): void
{
    session_add('flash-notes', $msg);
}

function flash_error($msg, $file, $line): void
{
    $ptr = (APP_ENV == 'DEV') ? '['.basename($file)."@$line] " : '';
    session_add('flash-errors', $ptr.$msg);
}

function flash_var($var): void
{
    session_add('flash-notes', '<pre>'.print_r($var, true).'</pre>');
}
