<?php declare(strict_types=1);

defined('MAX_IMAGE_SIZE') || define('MAX_IMAGE_SIZE', 1048576);

function img_save(array $file, string $name): bool
{
    if ($file['error'] || !getimagesize($file['tmp_name']) ||
        filesize($file['tmp_name']) > MAX_IMAGE_SIZE)
        return false;

	return move_uploaded_file($file['tmp_name'], $name);
}
