<?php declare(strict_types=1);

function error_404_page(): array
{
    header("HTTP/1.0 404 Not Found");
    return [
        'title'       => SITE_TITLE.' | Страница не найдена',
        'description' => 'Интернет-магазин Радиодетали. Страница не найдена.',
        'keywords'    => 'Page not found. Route 404',
        'page'        => 'error/404.phtml'
    ];
}
