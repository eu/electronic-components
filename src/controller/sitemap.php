<?php declare(strict_types=1);

require_once __MODEL__.'sitemap.php';
require_once __MODEL__.'catalog.php';

$v['view'] = 'sitemap.phtml';
$v['uri'] = [];

$catalog = catalog();
if (!$catalog)
    sitemap_error();

$v['uri'][] = 'about';
$v['uri'][] = 'base';

foreach ($catalog as $gid => $group) {
    $v['uri'][] = 'base/'.$group['path'];
    if (isset($group['has']))
        foreach ($group['has'] as $sgid => $sg)
            $v['uri'][] = 'base/'.$group['path'].'/'.$sg['path'];
}

$v['uri'] = sitemap_base();
