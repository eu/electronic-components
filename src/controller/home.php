<?php declare(strict_types=1);

require_once __MODEL__.'showcase.php';
require_once __MODEL__.'catalog.php';
require_once __MODEL__.'news.php';

function home_page(): array
{
    $v = [
        'title' => SITE_TITLE.' | Главная страница',
        'page'  => 'home.phtml'
    ];

    $page = (isset($_GET['p'])) ? (int) $_GET['p'] : 0;
    if ($page < 0)
        $page = 0;

    // Load showcase
    $v['showcase'] = showcase();
    if (!$v['showcase'])
        flash_error('Ошибка загрузки витрины!', __FILE__, __LINE__);

    // Load catalog tree
    $v['catalog'] = catalog();
    if (!$v['catalog'])
        flash_error('Ошибка загрузки каталога!', __FILE__, __LINE__);

    // Load news
    $pages = (int) news_pages();
    if (!$pages) {
        flash_error("Ошибка при подсчете страниц!", __FILE__, __LINE__);
        return $v;
    }

    if (!$page || $page > $pages)
        $page = $pages;

    $v['news'] = news($page, ($page == $pages));
    if (!$v['news']) {
        flash_error('Ошибка загрузки новостей!', __FILE__, __LINE__);
        return $v;
    }

    $v['pagination']['page']  = $page;
    $v['pagination']['pages'] = $pages;
    $v['pagination']['link'] = "?p=";

    return $v;
}

