<?php declare(strict_types=1);

require_once __INC__.'sanitize.php';

function whois(string $ip): ?string
{
	$sock = fsockopen(WHOIS_SERVER, 43, $errno, $errstr, 5);
	if (!$sock)
		return null;

	fputs($sock, $ip."\r\n");
    mb_internal_encoding("UTF-8");

    $out = '';
	while (!feof($sock)) {
        $out .= fgets($sock, 128);
   	}
    fclose ($sock);
    return $out;
}
