<?php declare(strict_types=1);

require_once __INC__.'db.php';
require_once __INC__.'cache.php';
require_once __INC__.'pagination.php';
require_once __INC__.'sanitize.php';

function news(int $page, bool $last): ?array
{
    $cache = cache_get("news_$page");

    if ($cache)
        return $cache;

    $sql = 'SELECT `id`, `link`, `name`, `date`, `img`, `info` '.
           'FROM `news` ORDER BY `id` ';

	if ($last)
		$sql .= "DESC LIMIT ".NEWS_ON_PAGE;
	else {
        $sql = "($sql ASC LIMIT ".(($page - 1)* NEWS_ON_PAGE).
               ', '.NEWS_ON_PAGE.") ORDER BY `id` DESC";
    }

    $res = db_query($sql);
    if (!$res || !db_num_rows($res))
        return null;

    $cache = [];
    while($news = db_fetch_assoc($res)) {
        $cache[] = [
            'id'    => (int) $news['id'],
            'link'  => s($news['link']),
            'name'  => s($news['name']),
            'date'  => s($news['date']),
            'img'   => ($news['img']) ? s($news['img']) : 'empty',
            'info'  => s($news['info'])
        ];
    }
    cache_save("news_$page", $cache);
    return $cache;
}

function news_pages(): int
{
    $cache = cache_get('news_pages');

    if ($cache)
        return $cache['pages'];

    $cache['pages'] = pagination_count_pages('news', NEWS_ON_PAGE);

    cache_save('news_pages', $cache);
    return $cache['pages'];
}

function news_by_id(int $id) : ?array
{
    $sql = 'SELECT `id`, `link`, `name`, `date`, `img`, `info` '.
           "FROM `news` WHERE `id`=$id LIMIT 1";

    $res = db_query($sql);
    if (!$res || !db_num_rows($res)) {
        return null;
    }

    $news = db_fetch_assoc($res);
	return [
		'id'    => (int) $news['id'],
		'link'  => s($news['link']),
		'name'  => s($news['name']),
		'date'  => s($news['date']),
		'img'   => ($news['img']) ? s($news['img']) : 'empty',
		'info'  => s($news['info'])
	];
}

function news_update(array &$news): bool
{
    extract($news);

    if ($id) {
        if ($img) {
            // Delete old image
            $sql = "SELECT `img` FROM `news` WHERE `id` = $id LIMIT 1";
            $res = db_query($sql);
            if (!$res || !db_num_rows($res))
                return false;

            $row = db_fetch_assoc($res);
            db_free($res);
            $img_file = 'img/news/'.
                        preg_replace("/[^a-z0-9\-_\.]+/i", "", $row['img']);
            if (file_exists($img_file))
                unlink($img_file);
        }
        $sql = "UPDATE `news` SET `link`='$link', ".
               "`name`='$name', `date`='$date', ".
               (($img) ? "`img`='$img', " : '').
               "`info`='$info' WHERE `id` = $id LIMIT 1";
    } else {
        $sql = 'INSERT INTO `news` '.
               '(`id`, `link`, `name`, `date`, `img`, `info`) '.
               "VALUES (NULL, '$link', '$name', '$date', '$img', '$info')";
    }
    if (!db_query($sql))
        return false;

    cache_delete('news_*');
    return true;
}

function news_delete(int $id): bool
{
    $sql = "SELECT `img` FROM `news` WHERE `id` = $id LIMIT 1";
    $res = db_query($sql);
    if (!$res || !db_num_rows($res))
        return false;

    $row = db_fetch_assoc($res);
    db_free($res);

    $sql = "DELETE FROM `news` WHERE `news`.`id` = $id";
    $res = db_query($sql);
    if (!$res)
        return false;

    $img = 'img/news/'.
           preg_replace("/[^a-z0-9\-_\.]+/i", "", $row['img']);
    if (file_exists($img))
        unlink($img);

    cache_delete('news_*');
    return true;
}

