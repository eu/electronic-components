<?php declare(strict_types=1);

require_once __INC__.'db.php';
require_once __INC__.'cache.php';
require_once __INC__.'sanitize.php';

function catalog(): ?array
{
    $tree = cache_get('catalog');
    if ($tree) return $tree;

    $sql = 'SELECT `gid`, `name`, `path` FROM `groups` ORDER BY `name` ASC';
    $res = db_query($sql);
    if (!$res || !db_num_rows($res))
        return null;

    $tree = [];
    while($group = db_fetch_assoc($res)) {
        $gid = (int) $group['gid'];
        $tree[$gid] = [
            'name' => s($group['name']),
            'path' => s($group['path'])
        ];
    }
    db_free($res);

    $sql = 'SELECT `gid`, `sgid`, `name`, `path` '.
           'FROM `subgroups` ORDER BY `name` ASC';
    $res = db_query($sql);
    if (!$res || !db_num_rows($res))
        return null;

    while($subgroup = db_fetch_assoc($res)) {
        $gid  = (int) $subgroup['gid'];
        $sgid = (int) $subgroup['sgid'];
        $tree[$gid]['has'][$sgid] = [
            'name' => s($subgroup['name']),
            'path' => s($subgroup['path'])
        ];
    }
    cache_save('catalog', $tree);
    return $tree;
}
