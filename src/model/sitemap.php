<?php declare(strict_types=1);

require_once __INC__.'db.php';
require_once __INC__.'cache.php';

function sitemap_error(): void
{
    require_once 'sitemap.xml';
    exit(1);
}

function sitemap_base(): iterator
{
    $c = cache_get('sitemap');

    if ($c)
        $base = $c;
    else {
        $res = db_query('SELECT `id` FROM `main`');

        if (!$res || !db_num_rows($res))
            sitemap_error();
        
        db_close();
        while ($row = db_fetch_row($res))
            $base[] = (int) $row[0];

        cache_save('sitemap', $base);
    }
    
    foreach ($base as $id)
        yield 'base/'.$id;
}

