<?php declare(strict_types=1);

require_once __INC__.'db.php';
require_once __INC__.'cache.php';
require_once __INC__.'sanitize.php';

function showcase(): ?array
{
    $sc = cache_get('showcase');

    if ($sc)
        return $sc;

    $sql = 'SELECT `id`, `type`, `img`, `wprice`, `rprice` FROM '.
           '`showcase` LEFT JOIN `main` ON `id`=`product` LIMIT 12';

    $res = db_query($sql);
    if (!$res || db_num_rows($res) !== 12)
        return null;

    $products = [];
    while($p = db_fetch_assoc($res)) {
        $products[] = [
            'id'    => (int) $p['id'],
            'type'  => s($p['type']),
            'img'   => ($p['img']) ? s($p['img']) : 'empty',
            'wprice'=> (float) $p['wprice'],
            'rprice'=> (float) $p['rprice']
        ];
    }
    cache_save('showcase', $products);
    return $products;
}

function showcase_update(int $place, int $id): bool
{
    $sql = "UPDATE `showcase` SET `product`='$id' ".
           "WHERE `showcase`.`place` = $place LIMIT 1";
    return db_query($sql);
}
