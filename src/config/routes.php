<?php declare(strict_types=1);
/*
 *  [ 'regexp', 'controller/file', 'func from controller (*_page | *_act)', ROLE ]
 */
return
[
    [ '^$',                         'home',             'home_page'                         ],
    [ '^about$',                    'about',            'about_page'                        ],

    [ '^base$',                     'base',             'base_search_page'                  ],
    [ '^base/(\d+)$',               'base',             'base_item_page'                    ],
    [ '^base/(\S+)/(\S+)$',         'base',             'base_groups_page'                  ],
    [ '^base/(\S+)$',               'base',             'base_group_page'                   ],

    [ '^cart$',                     'cart',             'cart_page'                         ],
    [ '^cart/add$',                 'cart',             'cart_add_act'                      ],
    [ '^cart/empty$',               'cart',             'cart_empty_act'                    ],
    [ '^cart/demo$',                'cart',             'cart_demo_act'                     ],

    [ '^profile/(\d+)$',            'profile',          'profile_view_page',    ROLE_USER   ],
    [ '^profile$',                  'profile',          'profile_page',         ROLE_USER   ],
    [ '^profile$',                  'profile',          'profile_nobody_page'               ],
    [ '^cy$',                       'profile',          'profile_cy_act'                    ],
    [ '^profile/(\d+)/save$',       'profile',          'profile_save_act',     ROLE_USER   ],
    [ '^profile/save$',             'profile',          'profile_new_act'                   ],
    [ '^profile/(\d+)/role$',       'profile',          'profile_role_act',     ROLE_ADMIN  ],

    [ '^order/(\d+)$',              'order',            'order_view_page',      ROLE_USER   ],
    [ '^order$',                    'order',            'order_create_page',    ROLE_USER   ],
    [ '^order/create$',             'order',            'order_create_act',     ROLE_USER   ],
    [ '^order/(\d+)/add_cart$',     'order',            'order_add_cart_act',   ROLE_USER   ],
    [ '^order/(\d+)/del$',          'admin/order',      'order_del_act',        ROLE_SELLER ],
    [ '^order/(\d+)/upd_info$',     'admin/order',      'order_upd_info_act',   ROLE_SELLER ],
    [ '^order/(\d+)/upd_num$',      'admin/order',      'order_upd_num_act',    ROLE_SELLER ],
    [ '^order',                     'order',            'order_nobody_act'                  ],

    [ '^logout$',                   'auth',             'auth_logout_act',      ROLE_USER   ],
    [ '^login$',                    'auth',             'auth_login_act'                    ],
    [ '^sitemap$',                  'sitemap',          ''                                  ],
 
    [ '^admin$',                    'admin/admin',      'admin_page',           ROLE_ADMIN  ],

    [ '',                           'error',            'error_404_page'                    ],
];

