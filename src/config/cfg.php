<?php declare(strict_types=1);

define('APP_LABEL', 'Radiodetali v.2.3');
define('APP_ENV', 'DEV'); // DEV | PROD
/*
 *  Paths
 */
if (APP_ENV === 'PROD') {
    define('ROOT', '/');
} else {
    define('ROOT', '/radiodetali/');
}
define('__INC__',   __SRC__.'include/');
define('__CACHE__', __SRC__.'cache/');
define('__MODEL__', __SRC__.'model/');
define('__VIEW__',  __SRC__.'view/');
define('__CONTROLLER__', __SRC__.'controller/');
/*
 * Cookie
 */
define('SESSID', 'RDSESSID');
define('COOKIE_NAME', 'RDCART');
define('COOKIE_LIFETIME', 86400);
define('MAX_IMAGE_SIZE', 1048576);
/*
 *  View
 */
define('SITE_TITLE', 'Радиодетали'); 
define(
    'SITE_URL',
    (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http").
    "://$_SERVER[HTTP_HOST]".ROOT
);
define(
    'SITE_DESCRIPTION',
    'Интернет-магазин Радиодетали.'.
    'Продажа оптом и в розницу.'
);
define(
    'SITE_KEYWORDS',
    'купить,радиодетали,интернет,магазин'.
    'электронные компоненты компоненты',
);
define('HOME_PAGE',     'home');
define('NEWS_ON_PAGE',  3);
define('ITEMS_ON_PAGE', 10);
define('ROWS_ON_PAGE',  20);
define('DATE_FORMAT',   '%d/%m/%Y %H:%i');
/*
 *  Whois
 */
define('WHOIS_SERVER', 'whois.ripe.net');
/*
 * DB
 */
define('DB_NAME',       'db_radiodetali');
define('DB_USER_NAME',  'db_user');
define('DB_ADMIN_NAME', 'db_admin');
if (APP_ENV === 'PROD') {
    define('DB_HOST',       'mysql.com');
    define('DB_USER_PASS',  '123');
    define('DB_ADMIN_PASS', '456');
    define('ORDER_MAIL',    'mail@mail.ru');
    define('MAILBOT',       'mailbot@mail.ru');
} else {
    define('DB_HOST',       'localhost');
    define('DB_USER_PASS',  '123');
    define('DB_ADMIN_PASS', '456');
}

date_default_timezone_set('Europe/Moscow');
