<?php declare(strict_types=1);

function app(): void
{
    require_once __SRC__.'app_init.php';
    /*
     * Router
     */
    $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri = preg_replace(['#^'.ROOT.'#', '#/$#'], '', $uri);
    $routes = require_once __SRC__.'config/routes.php';

    foreach ($routes as $route) {
        $role = (isset($route[3])) ? (int) $route[3] : ROLE_NOBODY;

        if ($v['role'] >= $role && preg_match('#'.$route[0].'#', $uri, $argv)) {
            array_shift($argv);
            foreach ($argv as $key => $arg) {
                if (!strncmp($arg, (string)(int) $arg, strlen($arg)))
                    $argv[$key] = (int) $arg;
            }
            require_once __CONTROLLER__.$route[1].'.php';
            if ($route[2]) {
                $v = array_replace_recursive(
                    $v,
                    call_user_func_array($route[2], $argv)
                );
            }
            break;
        }
    }
    $v['page_name'] = $route[1];
    require_once __SRC__.'app_close.php';
    /*
     * Render view
     * $v['view'] = index.phtml
     */
    (function() use ($v) {
        ob_start();
        require_once
        __VIEW__.(file_exists(__VIEW__.$v['view']) ? $v['view'] : '404.html');
        ob_end_flush();
    })();
}

function app_redirect(string $url): void
{
    header('Location: '.ROOT.$url);
    require_once __SRC__.'app_close.php';
    exit(0);
}

function app_return(): void
{
    if (!isset($_SERVER['HTTP_REFERER']) ||
	!substr_compare($_SERVER['HTTP_REFERER'], $_SERVER['REQUEST_URI'],
			-strlen($_SERVER['REQUEST_URI'])))
	    $ref = ROOT;
    else
	$ref = $_SERVER['HTTP_REFERER'];

    header("Location: $ref");
    require __SRC__.'app_close.php';
    exit(0);
}
